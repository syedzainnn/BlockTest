﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour {


    public void _MainScene()
    {
        SceneManager.LoadScene("Main");
    }

    public void _Datacom()
    {
        SceneManager.LoadScene("Datacom");
    }
    public void _MainMenu()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
