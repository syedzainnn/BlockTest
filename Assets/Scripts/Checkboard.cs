﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Checkboard : MonoBehaviour
{

    public TestingChild tC;
    Vector3 posW = new Vector3(0, 0, 2);
    Vector3 posS = new Vector3(0, 0, -2);
    Vector3 posA = new Vector3(-2, 0, 0);
    Vector3 posD = new Vector3(2, 0, 0);
    string instance_;
    public Text Hit;
    Animator anim;

    void Start() {
        anim = Hit.GetComponent<Animator>();
        anim.SetInteger("Hit", 0);
        tC = GameObject.Find("Table").GetComponent<TestingChild>();
        Hit.GetComponent<Text>();
        
        
        

    }


    private void LaneSwitching()
    {
        switch (instance_)
        {
            case "U":
                if (this.transform.localPosition.z < 4)
                {
                    this.transform.localPosition += posW;
                }
           
                break;

            case "D":
                if (this.transform.localPosition.z > -4)
                {
                    this.transform.localPosition += posS;
                }
                break;

            case "L":
                if (this.transform.localPosition.x > -4)
                {
                    this.transform.localPosition += posA;
                }
                break;

            case "R":
                if (this.transform.localPosition.x < 4)
                {
                    this.transform.localPosition += posD;
                }
                break;

            default:
                break;
        }    
    }


   private IEnumerator Move()
    {
        for (int i = 0; i <tC.ChilderenData .Length; i++)
        {
            if (tC.ChilderenData[i] != null)
            {
                instance_ = tC.ChilderenData[i].GetComponentInChildren<Text>().text;
                //Debug.Log(instance_);
                LaneSwitching();
                yield return new WaitForSeconds(1);
                if (Mathf.Round(this.transform.localPosition.x) == -4 && Mathf.Round(this.transform.localPosition.y) == 0)
                {
                    anim.SetInteger("Hit", 1);
                    Debug.Log("Target Hit");

                }
            }
        }
     
    }

   public void Submit()
    {
        StartCoroutine(Move());
    }


    public void Reset_()
    {
        SceneManager.LoadScene("Main");
    }


}
