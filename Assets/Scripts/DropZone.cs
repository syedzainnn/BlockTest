﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;


public class DropZone : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
    public bool trigger = false;
    GameObject cardinstance;
    public TestingChild tC_;

    void Start()
    {
        tC_ = GameObject.Find("Table").GetComponent<TestingChild>();
    }

    public void OnPointerEnter(PointerEventData eventData)

    {
        //Debug.Log("OnDrop to" + gameObject.name);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
       // Debug.Log("OnDrop to" + gameObject.name);
    }



    public void OnDrop(PointerEventData eventData)
    {
        trigger = true;
        Debug.Log(eventData.pointerDrag.name + "was dropped on" + gameObject.name);
        Draggable d = eventData.pointerDrag.GetComponent<Draggable>();

        if (d != null)
        {      
            d.parentToReturnTo = this.transform;
        }

    }
}
