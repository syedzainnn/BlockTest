﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class CommManager : MonoBehaviour
{
    public Text response;
    private string resp;
    private string IMG = "sproud.biz/SIT2/milky_way_stars_mountains_night_germany_bavaria_sky_45888_1920x1080.jpg";
    public RawImage rI;

    void Start()
    {
        response.text = "No Input";
    }

    IEnumerator GetText()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://www.sproud.biz/SIT2/Testing2.txt");
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            resp = www.downloadHandler.text;
            Debug.Log(www.downloadHandler.text);
            //resp = "Data has been changed";
            // Or retrieve results as binary data
            //byte[] results = www.downloadHandler.data;
            response.text = resp;
        }
    }



  

    IEnumerator GetIMG()
    {
        // Start a download of the given URL
        using (WWW www = new WWW(IMG))
        {
            yield return www;
            rI.GetComponent<Texture>();
            // assign texture
            //Renderer renderer = GetComponent<Renderer>();
            // renderer = rI.GetComponent<Renderer>();
            // renderer.material.mainTexture = www.texture;
            rI.texture = www.texture;
        }
    }






public void ImageComm()
    {
        StartCoroutine(GetIMG());
    }

    public void CorrectSelection()
    {
        StartCoroutine(GetText());
    }
}