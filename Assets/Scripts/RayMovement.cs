﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RayMovement : MonoBehaviour {
    private LineRenderer LR;
    public Transform LaserHit;

    void Start () {
        LR = GetComponent<LineRenderer>();
        LR.enabled = false;
        LR.useWorldSpace = true;

	}
	
	
	void Update () {
      
	}
    void Raycasting()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up);
        // Debug.DrawLine(sightStart.position, sightEnd.position, Color.green);
        LaserHit.position = hit.point;
    }

}
